function [ y ] = Untitled( FUN, STEP, LowerBound, UpperBound, IC )

x = LowerBound:STEP:UpperBound;
y = zeros(1,length(x));
% initial condition
y(1) = IC;

% calculation loop
for i=1:(length(x)-1)
    k_1 = FUN(x(i),y(i));
    k_2 = FUN(x(i)+0.5*STEP,y(i)+0.5*STEP*k_1);
    k_3 = FUN((x(i)+0.5*STEP),(y(i)+0.5*STEP*k_2));
    k_4 = FUN((x(i)+STEP),(y(i)+k_3*STEP));

    y(i+1) = y(i) + (1/6)*(k_1+2*k_2+2*k_3+k_4)*STEP;
end

end

