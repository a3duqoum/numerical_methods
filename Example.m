%Example on using Fun_RK 

%Define your function
 F_xy = @(t,r) exp(-t)*sin(t/(4*pi)) - 0.5*r; 

%Define the step you want to use 
step = 0.25; 

%Define your bounds on X 
LowerBound = 0;
UpperBound = 1;

%Define your IC 
IC = 0;

%Solve
Y = Fun_RK(F_xy,step,LowerBound,UpperBound,IC);